from coe_number.number_utils3 import check_triangle_type
import unittest

class TestCheckTriangleType(unittest.TestCase):
    def test1(self):
        result = check_triangle_type(5, 5, 5)
        self.assertEqual(result, "Equilateral Triangle")

    def test2(self): 
        result = check_triangle_type(5, 5, 7)
        self.assertEqual(result, "Isosceles Triangle")

    def test3(self):
        result = check_triangle_type(3, 4, 5)
        self.assertEqual(result, "Scalene Triangle")

    def test4(self):
        result = check_triangle_type(10, 1, 2)
        self.assertEqual(result, "Isosceles Triangle")

    def test5(self):
        result = check_triangle_type('mkefn', '@#4', '๑๒')
        self.assertEqual(result, "Isosceles Triangle")

