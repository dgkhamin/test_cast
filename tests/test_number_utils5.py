from coe_number.number_utils5 import add_number
import unittest

class TestAddNumber(unittest.TestCase):
    def test_add_integer_numbers(self):
        result = add_number("10", "20")
        self.assertEqual(result, "30")

    def test_add_decimal_numbers(self):
        result = add_number("10.5", "20.5")
        self.assertFalse(result)

    def test_add_invalid_input(self):
        result = add_number("abc", "def")
        self.assertFalse(result)

    def test_add_negative_number(self):
        result = add_number("-100", "200")
        self.assertEqual(result, "100")
