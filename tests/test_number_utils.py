from coe_number.number_utils import is_prime_list
import unittest
class PrimeListTest(unittest.TestCase):
    def test_give_1_2_3_is_prime(self):
        prime_list = [1, 2, 3]
        is_prime = is_prime_list(prime_list)
        self.assertTrue(is_prime)

    def test_give_7_9_11_is_prime(self):
        prime_list = [7, 9, 11]
        is_prime = is_prime_list(prime_list)
        self.assertTrue(is_prime)

    def test_give_1_2_3_Negative_number_is_prime(self):
        prime_list = [-1, -2, -3]
        is_prime = is_prime_list(prime_list)
        self.assertTrue(is_prime)

    def test_give_English_letters_is_prime(self):
        prime_list = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
        is_prime = is_prime_list(prime_list)
        self.assertFalse(is_prime)

    def test_give_Thai_letters_is_prime(self):
        prime_list = ['ก', 'ข', 'ฃ', 'ค', 'ฅ', 'ฆ', 'ง', 'จ', 'ฉ', 'ช', 'ซ', 'ฌ', 'ญ', 'ฎ', 'ฏ', 'ฐ', 'ฑ', 'ฒ', 'ณ', 'ด', 
                    'ต', 'ถ', 'ท', 'ธ', 'น', 'บ', 'ป', 'ผ', 'ฝ', 'พ', 'ฟ', 'ภ', 'ม', 'ย', 'ร', 'ล', 'ว', 'ศ', 'ษ', 'ส', 'ห', 'ฬ', 'อ', 'ฮ'
                    ,'ะ', 'ั', 'า', 'ำ', 'ิ', 'ี', 'ึ', 'ื', 'ุ', 'ู', 'ฺ', 'เ', 'แ', 'โ', 'ใ', 'ไ', 'ๅ', '็', '่', '้', '๊', '๋', '์','่', '้', '๊', '๋', '์']

        is_prime = is_prime_list(prime_list)
        self.assertFalse(is_prime)

    def test_give_Thai_letters_is_prime(self):
        prime_list =['๑', '๒', '๓', '๔', '๕', '๖', '๗', '๘', '๙', '๑๐']
        is_prime = is_prime_list(prime_list)
        self.assertFalse(is_prime)

    def test_give_Special_characters_is_prime(self):
        prime_list = special_characters = ['!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '-', '_',
                                            '=', '+', '[', ']', '{', '}', ';', ':', '<', '>', ',', '.', '/', '?', '|']
        is_prime = is_prime_list(prime_list)
        self.assertFalse(is_prime)
