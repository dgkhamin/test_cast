from coe_number.number_utils4 import find_pythagorean_triplet
import unittest


class TestPythagoreanTriplet(unittest.TestCase):
    def test_find_pythagorean_triplet(self):
        limit = 10
        expected_triplets = [(3, 4, 5), (6, 8, 10)]
        actual_triplets = find_pythagorean_triplet(limit)
        self.assertEqual(actual_triplets, expected_triplets)

    def test_find_pythagorean_triplet2(self):
        limit = 9
        expected_triplets = [(3, 4, 5), (6, 8, 10)]
        actual_triplets = find_pythagorean_triplet(limit)
        self.assertEqual(actual_triplets, expected_triplets)
