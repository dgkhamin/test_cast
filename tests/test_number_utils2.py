from coe_number.number_utils2 import is_thai_consonant
import unittest

class TestIsThaiConsonant(unittest.TestCase):
    def test_contains_thai1_consonant(self):
        self.assertTrue(is_thai_consonant("กบ"))

    def test_contains_thai2_consonant(self):
        self.assertTrue(is_thai_consonant("สวัสดีครับ"))

    def test_contains_thai3_consonant(self):
        self.assertTrue(is_thai_consonant("ดีจ้า"))

    def test_no_thai_consonant(self):
        self.assertFalse(is_thai_consonant("Hello World"))

    def test_empty_string(self):
        self.assertFalse(is_thai_consonant(""))

    def test_Special_characters(self):
        self.assertFalse(is_thai_consonant("@+_-"))
    
    def test_contains_number(self):
        self.assertTrue(is_thai_consonant(123))
