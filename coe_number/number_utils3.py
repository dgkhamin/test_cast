def check_triangle_type(side1, side2, side3):
    if side1.isdigit() and side2.isdigit() and side3.isdigit():
        if side1 == side2 == side3:
            return "Equilateral Triangle"  # สามเหลี่ยมด้านเท่า
        elif side1 == side2 or side1 == side3 or side2 == side3:
            return "Isosceles Triangle"  # สามเหลี่ยมด้านเท่า
        else:
            return "Scalene Triangle"  # สามเหลี่ยมด้านไม่เท่า
    return False
