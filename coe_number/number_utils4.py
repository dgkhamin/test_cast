def find_pythagorean_triplet(limit):
    triplets = []
    for a in range(1, limit):
        for b in range(a + 1, limit):
            c = (a**2 + b**2) ** 0.5
            if c.is_integer():
                triplets.append((a, b, int(c)))
    return triplets